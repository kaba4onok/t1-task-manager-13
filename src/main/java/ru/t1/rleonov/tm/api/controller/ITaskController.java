package ru.t1.rleonov.tm.api.controller;

import ru.t1.rleonov.tm.model.Task;

import java.util.List;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

    void renderTasks(List<Task> tasks);

    void showTaskById();

    void showTaskByIndex();

    void showTaskByProjectId();

    void removeTaskById();

    void removeTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

}
