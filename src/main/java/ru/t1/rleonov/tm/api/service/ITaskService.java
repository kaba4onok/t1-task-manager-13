package ru.t1.rleonov.tm.api.service;

import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.model.Task;
import java.util.List;

public interface ITaskService {

    Task add(Task task);

    Task create(String name);

    Task create(String name, String description);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task remove(Task task);

    List<Task> findAll();

    List<Task> findAllByProjectId(String projectId);

    void clear();

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

}
