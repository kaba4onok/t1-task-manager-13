package ru.t1.rleonov.tm.api.repository;

import ru.t1.rleonov.tm.model.Task;
import java.util.List;

public interface ITaskRepository {

    Integer getSize();

    Task add(Task task);

    List<Task> findAll();

    List<Task> findAllByProjectId (String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    void clear();

}
