package ru.t1.rleonov.tm.api.service;

import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.model.Project;
import java.util.List;

public interface IProjectService {

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project remove(Project project);

    List<Project> findAll();

    void clear();

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

}
