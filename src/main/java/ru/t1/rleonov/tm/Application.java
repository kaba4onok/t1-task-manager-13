package ru.t1.rleonov.tm;

import ru.t1.rleonov.tm.component.Bootstrap;

public final class Application {

    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.runApplication(args);
    }

}
